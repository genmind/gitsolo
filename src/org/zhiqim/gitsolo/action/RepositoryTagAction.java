/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.git.objer.objectid.ObjectId;
import org.zhiqim.git.objer.objectid.RevCommit;
import org.zhiqim.git.refer.Ref;
import org.zhiqim.gitsolo.dao.GitMemberDao;
import org.zhiqim.gitsolo.dao.GitProjectDao;
import org.zhiqim.gitsolo.model.TagModel;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsAlphaNumericDotLen;

/**
 * 标签列表
 *
 * @version v1.0.0 @author zouzhigang 2016-11-1 新建与整理
 */
public class RepositoryTagAction extends StdSwitchAction implements GitConstants
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsAlphaNumericDotLen("tagName", "标签名称为字母数字和点号组成，[1, 20]位", 1, 20));
    }
    
    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsAlphaNumericDotLen("tagName", "标签名称为字母数字和点号组成，[1, 20]位", 1, 20));
    }
    
    @Override
    protected void list(HttpRequest request) throws Exception
    {
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        List<Ref> refList = git.tagList().call();
        
        List<TagModel> list = new ArrayList<>();
        for (Ref ref : refList)
        {
            ObjectId oid = ref.getObjectId();
            RevCommit commit = git.resolve(oid, RevCommit.class);
            list.add(new TagModel().setRef(ref).setCommit(commit));
        }
        
        request.setAttribute("list", list);
    }
    
    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }
    
    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        String tagName = request.getParameter("tagName");
        
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        git.tag().setName(tagName).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "创建了标签", tagName);
    }
    
    @Override
    protected void modify(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        String oldTagName = request.getParameter("oldTagName");
        String tagName = request.getParameter("tagName");
        if(oldTagName.equals(tagName))
            return;
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        List<Ref> refList = git.tagList().call();
        for (Ref ref : refList)
        {
            String brancheName1 = ref.getName().substring(P_REFS_TAGS_.length());
            if(brancheName1.equals(tagName))
            {
                request.setResponseError("该标签已存在!");
                return;
            }
        }
        git.tagRename().setNewName(tagName).setOldName(oldTagName).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "修改了标签", tagName);
    }
    
    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        String tagName = request.getParameter("tagName");
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        git.tagDelete().setTags(tagName).call();
        
        GitMemberDao.report(request, GitProjectDao.getProjectId(request), request.getSessionName(), "删除了标签", tagName);
    }
}
