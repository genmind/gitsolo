/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.action.portal;

import org.zhiqim.git.Git;
import org.zhiqim.git.GitConstants;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.HttpResponse;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.util.Validates;

/**
 * 引用信息（客户端查询引用表）
 *
 * @version v1.0.0 @author zouzhigang 2017-11-27 新建与整理
 */
public class InfoRefsAction implements Action, GitConstants
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        HttpResponse response = request.getResponse();
        
        String service = request.getParameter("service");
        if (Validates.isEmptyBlank(service))
        {//服务编号
            response.sendError(_400_BAD_REQUEST_);
            return;
        }
        
        //请求正确，设置响应类型
        response.setContentTypeNoCharset(infoRefsResultType(service));
        
        Git git = (Git)request.getAttribute(GIT_ATTRIBUTE_REPOSITORY);
        git.sendAdvertisedRefs(response.getOutputStream(), service);
    }
    
    /**
     * 组装infoRefs响应类型
     * 
     * @param service           服务名称
     * @return                  响应类型
     */
    public static String infoRefsResultType(String service)
    {
        return "application/x-" + service + "-advertisement";
    }
}
