/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用木兰宽松许可证第2版]
 * 
 * https://zhiqim.org/project/zhiqim_products/gitsolo.htm
 *
 * gitsolo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.gitsolo.dbo;

import java.io.Serializable;
import java.sql.Timestamp;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 项目信息表 对应表《GIT_PROJECT》
 */
@AnAlias("GitProject")
@AnNew
@AnTable(table="GIT_PROJECT", key="PROJECT_ID", type="InnoDB")
public class GitProject implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="PROJECT_ID", type="long", notNull=true)    private long projectId;    //1.项目编号
    @AnTableField(column="PROJECT_NAME", type="string,64", notNull=true)    private String projectName;    //2.项目名称
    @AnTableField(column="PROJECT_STATUS", type="byte", notNull=true)    private int projectStatus;    //3.项目状态，0：正常，1：停用
    @AnTableField(column="PROJECT_SEQ", type="int", notNull=true)    private int projectSeq;    //4.项目排序数
    @AnTableField(column="PROJECT_MANAGER", type="string,32", notNull=true)    private String projectManager;    //5.项目组长
    @AnTableField(column="PROJECT_BEGIN_DATE", type="string,10,char", notNull=true)    private String projectBeginDate;    //6.项目开始时间
    @AnTableField(column="PROJECT_END_DATE", type="string,10,char", notNull=true)    private String projectEndDate;    //7.项目结束时间
    @AnTableField(column="PROJECT_CREATED", type="datetime", notNull=true)    private Timestamp projectCreated;    //8.项目创建时间
    @AnTableField(column="PROJECT_MODIFIED", type="datetime", notNull=true)    private Timestamp projectModified;    //9.项目修改时间
    @AnTableField(column="PROJECT_DESC", type="string,512", notNull=false)    private String projectDesc;    //10.项目描述

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(long projectId)
    {
        this.projectId = projectId;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public int getProjectStatus()
    {
        return projectStatus;
    }

    public void setProjectStatus(int projectStatus)
    {
        this.projectStatus = projectStatus;
    }

    public int getProjectSeq()
    {
        return projectSeq;
    }

    public void setProjectSeq(int projectSeq)
    {
        this.projectSeq = projectSeq;
    }

    public String getProjectManager()
    {
        return projectManager;
    }

    public void setProjectManager(String projectManager)
    {
        this.projectManager = projectManager;
    }

    public String getProjectBeginDate()
    {
        return projectBeginDate;
    }

    public void setProjectBeginDate(String projectBeginDate)
    {
        this.projectBeginDate = projectBeginDate;
    }

    public String getProjectEndDate()
    {
        return projectEndDate;
    }

    public void setProjectEndDate(String projectEndDate)
    {
        this.projectEndDate = projectEndDate;
    }

    public Timestamp getProjectCreated()
    {
        return projectCreated;
    }

    public void setProjectCreated(Timestamp projectCreated)
    {
        this.projectCreated = projectCreated;
    }

    public Timestamp getProjectModified()
    {
        return projectModified;
    }

    public void setProjectModified(Timestamp projectModified)
    {
        this.projectModified = projectModified;
    }

    public String getProjectDesc()
    {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc)
    {
        this.projectDesc = projectDesc;
    }

}
